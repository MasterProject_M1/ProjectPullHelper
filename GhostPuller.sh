#!/bin/bash

###########################################################################
# This script will pull every single projects (have to be already cloned) #
###########################################################################

# NLib projects
cd NLib/NLib
pwd
git pull
cd ../NHTTP
pwd
git pull
cd ../Test/NClientSFTP
pwd
git pull
cd ../NClientSSH
pwd
git pull
cd ../../..

# Parser projects
cd NParser/NJson
pwd
git pull
cd ../NParser
pwd
git pull
cd ../NYaml
pwd
git pull
cd ../..

# Device projects
cd NDevice/NDeviceScanner
pwd
git pull
cd ../NDeviceCommon
pwd
git pull
cd ../NDeviceHUE
pwd
git pull
cd ../NDeviceMFI
pwd
git pull
cd ../NDeviceGhost/NGhostClient
pwd
git pull
cd ../NGhostAudio
pwd
git pull
cd ../NGhostCommon
pwd
git pull
cd ../NGhostRFID
pwd
git pull
cd ../NGhostWatcher
pwd
git pull
cd ../../..
