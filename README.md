Architecture ghost
==================

Ghost watcher
-------------

Le ghost watcher est un programme ayant comme objectif de recevoir toutes les mises à jour
d'état des différents sous services. Une fois lancé, celui-ci doit être détecté par le ghost
client, et son adresse doit être broadcastée à tous les services ghost pour qu'ils l'enregistrent
en temps qu'adresse à contacter pour transmettre leurs évenements.

Stoque une pile d'évenements.

Ghost client
------------

Le ghost client est un programme permettant de scanner le réseau à la recherche
de périphériques du type Ghost, ou d'autres périphériques asservis (Exemple: Philips HUE).

Dans le cas des périphériques asservis comme HUE, le ghost client se charge de se maintenir au courant
de leur état actuel, et fait office de transmetteur vers le callback `Ghost watcher`.

Le ghost client génère un ensemble identifiant action suivant les périphériques détectés.

Pour des actions complexes, il ajoute une liste de paramètres potentiels.

Il créé des phrases textuelles associées à l'action à effectuer.

Ghost RFID
----------

Le ghost RFID est un programme pouvant ouvrir un lecteur RFID en liaison serie, et capable de récupérer
l'identifiant d'un badge éventuellement passé devant. Si il est lié à un `Ghost watcher`, il peut ensuite
transmettre l'identifiant pour un traitement éventuel.

Ghost Audio
-----------

Le ghost audio est un programme permettant de lire des sons, et pouvant gérer la synchronisation entre
plusieurs périphériques distants.

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/ProjectPullHelper


