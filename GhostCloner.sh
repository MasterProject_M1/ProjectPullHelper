#!/bin/bash

#######################################################
# This script will clone the whole Ghost project tree #
#######################################################

USER=NLucasSoares

# NLib projects
mkdir NLib
cd NLib
git clone https://$USER@gitlab.com/NLibProject/NLib.git
git clone https://$USER@gitlab.com/NLibProject/NHTTP.git
mkdir Test
cd Test
git clone https://$USER@gitlab.com/NLibProject/Test/NClientSFTP.git
git clone https://$USER@gitlab.com/NLibProject/Test/NClientSSH.git
cd ../..

# Parser projects
mkdir NParser
cd NParser
git clone https://$USER@gitlab.com/MasterProject_M1/NParser/NYaml.git
git clone https://$USER@gitlab.com/MasterProject_M1/NParser/NJson.git
git clone https://$USER@gitlab.com/MasterProject_M1/NParser/NParser.git
cd ..

# Device projects
mkdir NDevice
cd NDevice
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceScanner.git
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceCommon.git
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceHUE.git
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceMFI.git
mkdir NDeviceGhost
cd NDeviceGhost
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostClient.git
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostAudio.git
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostCommon.git
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostRFID.git
git clone https://$USER@gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostWatcher.git
cd ../..

# CMakeLists.txt
echo -e "# Set minimal cmake version\ncmake_minimum_required( VERSION\n\t3.1.3 )\n\n# Projet NJson\nadd_subdirectory( NParser/NJson/ )\n\n# Projet NYaml\nadd_subdirectory( NParser/NYaml/ )\n\n# Projets test NLib\nadd_subdirectory( NLib/Test/NClientSSH )\n\n# Projets test NLib\nadd_subdirectory( NLib/Test/NClientSFTP )\n\n# Device scanner\nadd_subdirectory( NDevice/NDeviceScanner )\n\n# HUE\nadd_subdirectory( NDevice/NDeviceHUE )\n\n# MFI\nadd_subdirectory( NDevice/NDeviceMFI )\n\n# Ghost listener\nadd_subdirectory( NDevice/NDeviceGhost/NGhostClient )\n\n# Ghost audio\nadd_subdirectory( NDevice/NDeviceGhost/NGhostAudio )\n\n# Ghost RFID\nadd_subdirectory( NDevice/NDeviceGhost/NGhostRFID )\n\n# Ghost watcher\nadd_subdirectory( NDevice/NDeviceGhost/NGhostWatcher )\n\n# HTTP Server\nadd_subdirectory( NLib/NHTTP )" > CMakeLists.txt
