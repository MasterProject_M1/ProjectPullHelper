#!/bin/bash

###############################################
# This script will push every single projects #
###############################################

# NLib projects
cd NLib/NLib
pwd
git diff
git add -A
git commit
git push
cd ../NHTTP
pwd
git diff
git add -A
git commit
git push
cd ../Test/NClientSFTP
pwd
git diff
git add -A
git commit
git push
cd ../NClientSSH
pwd
git diff
git add -A
git commit
git push
cd ../../..

# Parser projects
cd NParser/NJson
pwd
git diff
git add -A
git commit
git push
cd ../NParser
pwd
git diff
git add -A
git commit
git push
cd ../NYaml
pwd
git diff
git add -A
git commit
git push
cd ../..

# Device projects
cd NDevice/NDeviceScanner
pwd
git diff
git add -A
git commit
git push
cd ../NDeviceCommon
pwd
git diff
git add -A
git commit
git push
cd ../NDeviceHUE
pwd
git diff
git add -A
git commit
git push
cd ../NDeviceMFI
pwd
git diff
git add -A
git commit
git push
cd ../NDeviceGhost/NGhostClient
pwd
git diff
git add -A
git commit
git push
cd ../NGhostAudio/
git diff
git add -A
git commit
git push
cd ../NGhostCommon/
git diff
git add -A
git commit
git push
cd ../NGhostRFID/
git diff
git add -A
git commit
git push
cd ../NGhostWatcher/
git diff
git add -A
git commit
git push
cd ../../..
